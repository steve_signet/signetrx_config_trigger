/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.trigger.signetrx_config_trigger;

import com.mysql.jdbc.Connection;
import com.signet.signetrx.trigger.Rxinfo;
import com.signet.signetrx.trigger.SignetrxConfigTrigger;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class SignetrxConfigTriggerTest {
    private static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";    
    private static final String URL = "jdbc:mysql://localhost/steve_schema";
    private static final String USER = "root";
    private static final String PASSWORD = "muonres";

    private Statement statement;
    private Connection connection;
    private String fileName;
    private String testFileName;
    private SignetrxConfigTrigger instance;
    private String customerId;                 
    
    public SignetrxConfigTriggerTest() {
    }
    
    @Before
    public void setUp() {
        fileName = "test/com/signet/signetrx/trigger/signetrx_config_trigger/100130_Central_RxInfo.ini";
        testFileName = "test/com/signet/signetrx/trigger/signetrx_config_trigger/test.ini";
        customerId = "1001"; 
        instance = new SignetrxConfigTrigger();      
        connection = null;
                
        try {
            Class.forName(DB_DRIVER_CLASS).newInstance();
            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            statement = connection.createStatement();     
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }             
    }
    
    @After
    public void tearDown() {
        try {          
            statement.close();
            connection.close();           
        } catch (SQLException se) {
            System.out.println(se.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }                
    }

    /**
     * Test of readRxinfoData method, of class SignetrxConfigTrigger.
     */
    @Test
    public void testGetRxinfoData() {
        System.out.println("testGetRxinfoData");
         
        String expResult = "4040";        
        String result = instance.readRxinfoData(testFileName);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateRxinfo method, of class SignetrxConfigTrigger.
     */
    @Test
    public void testRun() {
        System.out.println("testRun");
        
        String rxinfoData = "2031";

        try {
            Rxinfo beforeRxinfo = instance.queryRxinfo(statement, customerId);
            String beforeRxinfoData = beforeRxinfo.getRxinfoData();
            String beforeCreateDate = beforeRxinfo.getCreateDate();

            Rxinfo updatedRxinfo = instance.updateRxinfo(statement, customerId, rxinfoData);
            String updatedRxinfoData = updatedRxinfo.getRxinfoData();
                
            // Test that rxinfo table has been updated correctly
            assertEquals(updatedRxinfoData, rxinfoData);
            
            Rxinfo afterOldRxinfo = instance.queryOldRxinfo(statement, customerId);
            String afterOldRxinfoData = afterOldRxinfo.getRxinfoData();
            String afterOldCreateDate = afterOldRxinfo.getCreateDate();

            // Test that old_rxinfo table has been updated correctly
            assertEquals(beforeRxinfoData, afterOldRxinfoData);

            // Test that create_date has been updated correctly in old_rxinfo table
            assertEquals(beforeCreateDate, afterOldCreateDate);
        } catch (SQLException se) {
            fail("SQLException thrown: " + se.getLocalizedMessage());
        }      
    }

    /**
     * Test of updateRxinfo method, of class SignetrxConfigTrigger.
     */
    @Test
    public void testUpdateRxinfo() {
        System.out.println("testUpdateRxinfo");
        
        String rxinfoData = instance.readRxinfoData(testFileName);
                
        try {
            String expResult = rxinfoData;
            Rxinfo rxinfo = instance.updateRxinfo(statement, customerId, rxinfoData);
            String result = rxinfo.getRxinfoData();
            assertEquals(expResult, result);       
        } catch (SQLException se) {
            fail("SQLException: " + se.getMessage());
        }
    }
}