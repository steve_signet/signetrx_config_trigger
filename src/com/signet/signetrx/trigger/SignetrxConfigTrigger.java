/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.trigger;

import com.mysql.jdbc.Connection;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.signet.commons.logging.LogWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author sburns
 */
public class SignetrxConfigTrigger {
    private static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";    
    private static final String URL = "jdbc:mysql://localhost/steve_schema";
    private static final String USER = "root";
    private static final String PASSWORD = "muonres";

    private LogWriter log = com.signet.signetrx.trigger.SignetrxConfigTriggerLogWriter.getInstance();
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SignetrxConfigTrigger signetrxConfigTrigger = new SignetrxConfigTrigger();
        signetrxConfigTrigger.run();
    }

    public void run() {
        String customerId = "1001";                 
        String fileName = "test/com/signet/signetrx/trigger/signetrx_config_trigger/100130_Central_RxInfo.ini";
        //String fileName = "test/com/signet/signetrx/trigger/signetrx_config_trigger/test.ini";
        
        Connection connection = null;
                
        try {
            // Load the Driver.
            Class.forName(DB_DRIVER_CLASS).newInstance();
            
            // Connect to the database.
            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            
            // Create statement
            Statement statement = connection.createStatement();
             
            String rxinfoData = readRxinfoData(fileName);

            if (rxinfoData != null) {
                Rxinfo currentRxinfo = queryRxinfo(statement, customerId);
                String currentRxinfoData = currentRxinfo.getRxinfoData();
                
                if (rxinfoData.equals(currentRxinfoData) == false) {
                    Rxinfo updatedRxinfo = updateRxinfo(statement, customerId, rxinfoData);
                } 
            } else {
                System.out.println("Unable to read rxinfo_data from file: " + fileName);
            }
    
            statement.close();
            connection.close();           
        } catch (SQLException se) {
            System.out.println("SQLException thrown: " + se.getMessage());
        } catch (Exception e) {
            System.out.println("Exception thrown: " + e.getMessage());
        }     
    }
    
    /**
     * 
     * @param fileName  rxinfo file to read data from
     * @return String rxinfoData
     */
    public String readRxinfoData(String fileName) {
        String rxinfoData = null;
        
        try {            
            File file = new File(fileName);
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] data = new byte[(int)file.length()];
            fileInputStream.read(data);
            fileInputStream.close();

            rxinfoData = new String(data, "UTF-8"); 
            
            System.out.println(rxinfoData);
 
            return rxinfoData;                    
         } catch (IOException ex) {
            log.error("Error processing RxInfo file: " + ex.getMessage());
            return rxinfoData;
        }           
    }
    
    /**
     * 
     * @param statement
     * @param customerId
     * @param rxinfoData
     * @return updated rxinfo data
     * @throws SQLException 
     */
    public Rxinfo updateRxinfo(Statement statement, String customerId, String rxinfoData) throws SQLException {
        Rxinfo rxinfo = new Rxinfo();

        String updateSQL = "UPDATE steve_schema.rxinfo SET rxinfo_data = '" + rxinfoData + "', last_modified = now() WHERE customer_id = " + customerId;
        int rowsUpdated = statement.executeUpdate(updateSQL);

        if (rowsUpdated == 1) {
            String querySQL = "SELECT * FROM steve_schema.rxinfo";
            ResultSet resultSet = statement.executeQuery(querySQL);            

            if (resultSet != null) {
                boolean isFirst = resultSet.first();

                if (isFirst == true) {
                    rxinfo.setCustomerId(resultSet.getString("customer_id"));
                    rxinfo.setRxinfoData(resultSet.getString("rxinfo_data"));
                    rxinfo.setUserId(resultSet.getString("user_id"));
                    rxinfo.setCreateDate(resultSet.getString("create_date"));
                    rxinfo.setLastModified(resultSet.getString("last_modified"));
                    rxinfo.setActiveFlag(resultSet.getInt("active_flag"));
                }
            }            
        } else {
            System.out.println("No rows updated");
        }
            
        return rxinfo;
    }
    
    /**
     * 
     * @param statement
     * @return rxinfo data
     * @throws SQLException 
     */
    public Rxinfo queryRxinfo(Statement statement, String customerId) throws SQLException {
        Rxinfo rxinfo = new Rxinfo();

        // Test old_rxinfo before update
        String querySQL = "SELECT * FROM steve_schema.rxinfo WHERE customer_id = '" + customerId + "'";
        ResultSet resultSet = statement.executeQuery(querySQL);            

        if (resultSet != null) {
            boolean isFirst = resultSet.first();

            if (isFirst == true) {
                rxinfo.setCustomerId(resultSet.getString("customer_id"));
                rxinfo.setRxinfoData(resultSet.getString("rxinfo_data"));
                rxinfo.setUserId(resultSet.getString("user_id"));
                rxinfo.setCreateDate(resultSet.getString("create_date"));
                rxinfo.setLastModified(resultSet.getString("last_modified"));
                rxinfo.setActiveFlag(resultSet.getInt("active_flag"));
             }
        }
                    
        return rxinfo;
    }
    
    /**
     * 
     * @param statement
     * @return rxinfo data
     * @throws SQLException 
     */
    public Rxinfo queryOldRxinfo(Statement statement, String customerId) throws SQLException {
        Rxinfo rxinfo = new Rxinfo();

        // Test old_rxinfo before update
        String querySQL = "SELECT * FROM steve_schema.old_rxinfo WHERE customer_id = '" + customerId + "' ORDER BY last_modified DESC";
        ResultSet resultSet = statement.executeQuery(querySQL);            

        if (resultSet != null) {
            boolean isFirst = resultSet.first();

            if (isFirst == true) {
                rxinfo.setCustomerId(resultSet.getString("customer_id"));
                rxinfo.setRxinfoData(resultSet.getString("rxinfo_data"));
                rxinfo.setUserId(resultSet.getString("user_id"));
                rxinfo.setLastModified(resultSet.getString("last_modified"));
                rxinfo.setCreateDate(resultSet.getString("create_date"));
                rxinfo.setActiveFlag(resultSet.getInt("active_flag"));
             }
        }
                    
        return rxinfo;
    }
}
