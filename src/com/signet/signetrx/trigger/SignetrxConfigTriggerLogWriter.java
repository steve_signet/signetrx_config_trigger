/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.trigger;

import com.signet.commons.logging.LogWriter;
import java.net.URL;
import org.apache.log4j.LogManager;

/**
 *
 * @author sburns
 */

public class SignetrxConfigTriggerLogWriter extends LogWriter {
    
    private static final URL configUrl = ClassLoader.getSystemResource("com/signet/signetrx/trigger/logging.properties");
    
    private static final com.signet.signetrx.trigger.SignetrxConfigTriggerLogWriter instance = 
            new com.signet.signetrx.trigger.SignetrxConfigTriggerLogWriter();
    
    public SignetrxConfigTriggerLogWriter() {
        super(configUrl);
        super.addLogger(LogManager.getLogger("AppLogFile"));
        super.addLogger(LogManager.getLogger("SystemOut"));
        super.addLogger(LogManager.getLogger("TextPane"));
    }
    
    public static com.signet.signetrx.trigger.SignetrxConfigTriggerLogWriter getInstance() {
        return instance;
    }
}
