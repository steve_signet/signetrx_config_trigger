CREATE TRIGGER rxinfo_backup_trigger BEFORE UPDATE ON rxinfo
FOR EACH ROW 
INSERT INTO old_rxinfo (customer_id, rxinfo_data, user_id, create_date, active_flag)
VALUES(old.customer_id, old.rxinfo_data, old.user_id, old.create_date, 0)

