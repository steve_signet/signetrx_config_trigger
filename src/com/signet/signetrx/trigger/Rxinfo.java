/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.trigger;

/**
 *
 * @author sburns
 */
public class Rxinfo {
    private String customerId;
    private String rxinfoData;
    private String userId;
    private String createDate;
    private String lastModified;
    private int activeFlag;

    /**
     * @return the customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the rxinfoData
     */
    public String getRxinfoData() {
        return rxinfoData;
    }

    /**
     * @param rxinfoData the rxinfoData to set
     */
    public void setRxinfoData(String rxinfoData) {
        this.rxinfoData = rxinfoData;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the createDate
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the lastModified
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * @param lastModified the lastModified to set
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * @return the activeFlag
     */
    public int getActiveFlag() {
        return activeFlag;
    }

    /**
     * @param activeFlag the activeFlag to set
     */
    public void setActiveFlag(int activeFlag) {
        this.activeFlag = activeFlag;
    }   
}
